(function(){
  'use strict';
  /**@jsdoc - Bootstrapping all modules for star docX
   *
   */
    angular
      .module('starDocXApp',
        [
          'app.core',
          'app.common',
          'app.dashboard',
          'app.directives',
          'app.layout',
          'app.facebook'
        ]
      );
})();
