(function(){
  "use strict";
  function Dashboard($log){
    var service = {
      data: _getData
    };

    function _getData(){
      $log.info('Inside get Data');
    }

    return service;
  }

  Dashboard.$inject = ['$log'];

  angular
    .module('app.dashboard')
    .factory('dashboardService', Dashboard);


})();
