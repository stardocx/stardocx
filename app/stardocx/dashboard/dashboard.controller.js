(function(){
  "use strict";
  function Dashboard(){
    var vm = this;
    vm.title = "This is the landing page";

  }

  Dashboard.$inject = [];
  angular
    .module('app.dashboard')
    .controller('Dashboard', Dashboard);

})();
