(function(){
  "use strict";
  function configureRoutes($stateProvider, $urlRouterProvider, $httpProvider){
    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('notices', {
        url: '/notices',
        templateUrl: 'stardocx/dashboard/landing.html'
      });
  }
  configureRoutes.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
  angular
    .module('app.dashboard')
    .config(configureRoutes);

})();
