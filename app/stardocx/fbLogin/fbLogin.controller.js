(function IIFE(){

  "use strict";
  function FBLogin(ezfb, $localStorage){
    var vm = this;

    vm.authenticateFBLogin = function(){
      ezfb.login().then(fbSuccess, fbFailure);

        function fbSuccess(res) {
          $localStorage.fbInfo = res;
          //console.info(JSON.stringify(res));
        };

      function fbFailure(res) {
        $localStorage.fbInfo = null;
        //console.info(JSON.stringify(res));
      };
    }

  }

  FBLogin.$inject = ['ezfb', '$localStorage'];
  angular
    .module("app.facebook")
    .controller("FBLogin", FBLogin);

})();
