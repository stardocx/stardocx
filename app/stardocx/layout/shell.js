(function() {
    'use strict';
    function Shell() {
        /*jshint validthis: true */
        var vm = this;
        vm.title = 'StarDocX';
    }

  Shell.$inject = [];
  angular
    .module('app.layout')
    .controller('Shell', Shell);
})();
