(function(){
  'use strict';

  function CommonConfig($httpProvider){
    $httpProvider.interceptors.push('authInterceptor');
  }

  CommonConfig.$inject = ['$httpProvider'];

  angular
    .module('app.common',[])
    .config(CommonConfig);

})();
