(function InterceptorIIFE(){

  function authInterceptor($q, $localStorage, $log){

    var interceptor = {
      request: requestHandler,
      response: responseHandler,
      responseError: responseErrorHandler,
    };

    function requestHandler(config){
      var facebookInfo = $localStorage.fbInfo;
      if(facebookInfo && facebookInfo.status === "connected" && facebookInfo.authResponse && facebookInfo.authResponse.accessToken){
        var token = facebookInfo.authResponse.accessToken;
        if(token){
          config.headers.Authorization = 'Bearer'+token;
        }else{

        }
      }
      return config;
    }

    function responseHandler(response){
      return response;
    }

    function responseErrorHandler(response){
      return $q.reject(response);
    }
    return interceptor;
  }

  authInterceptor.$inject = ['$q','$localStorage', '$log'];
  angular.module('app.common')
    .factory('authInterceptor', authInterceptor);
})();
