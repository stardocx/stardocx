(function(){
  'use strict';
  angular.module('app.core',
    [
      'ngAnimate',
      'ngCookies',
      'ngResource',
      'ngRoute',
      'ngSanitize',
      'ngTouch',
      'ngStorage',
      'ui.bootstrap',
      'ui.select',
      'ui.router'
    ]
  );


})();
