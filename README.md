------------------------------------------------------------------------------------------------**Please Do Read ME**-------------------------------------------------------------------------------------------------------------


This README is an evolving document that will be updated over the course of star docx. It's aimed to serve as starting point for new developers as well as for existing developers.
This is open for suggestions and can be added/updated after discussing with in team.


(Code Inspired by John Papa style guide and Todd Motto style guide).


https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md


https://github.com/toddmotto/angular-styleguide/tree/angular-old-es5

Try to divide app in different modules/components and final app module will be a collection of multiple small modules. 


So in a nutshell app.js will only have reference to multiple smaller modules.


Advantage – It gives us the flexibility of enabling/disabling modules as per their status or as per their completion plan which is not possible if we have one single module.


**It’s always better to use LIFT principle for app where


L – Locating of files is easy
I – Identfy code at glance
F – Flat
T -  Try to stay DRY (Don’t Repeat yourself)**

All the url's are not present in one app.js file and there is no single common folder for views. 

Url's and their templates will be available in individual modules.

Benefit - Better readability and easy to identify files.

To run your workspace - 
  * Do npm install.
*   Do bower install
  After bower install is done – 

Do bower uninstall jquery.  We are doing this because bootstrap downloads latest version of jQuery(3) which is incompatible with bootstrap.

After uninstalling jquery go to bower_components/bootstrap-sass-official -> .bower.json 

and replace  "jquery": ">= 1.9.0" with  "jquery": "1.9.1 - 2"

After replacing run bower install again.

Typically in an angular app we have two options for our folder structure.

1.	Folders by Type
2.	Folders by Features

Here at star docx, we will use folders by features and may follow hybrid structure ( folders by feature and subfolders by type) if folder structure starts growing too much.

File Naming Convention used –

1.	Controllers - controllerName.controller.js

2.	Service - serviceName.service.js

3.	Module - moduleName.module.js

4 	Directives – directiveName.directive.js
  
Controller are named as Auth.js and not AuthCtrl.js.

Things to note – 

1. Controllers will follow Pascal case (Followed by capital letter) as controllers are served as a new constructor object every time.

2. We often see terms like Ctrl or Controller (AuthCtrl, AuthController) used with controllers, but we can avoid this by just naming it like Auth. 

Reason - We often use controllers like ng-controller = "AuthCtrl as auth" or angular.controller("AuthCtrl", AuthCtrl where its self-explanatory that AuthCtrl is a controller. To avoid this redundancy we can simply use Auth.

3. For others services and directives we will use camelCase letters

Rather than having all the routes defined in one single app file all the routes will be defined in their respective modules. This ensures readability and we can easily turn on/off any routes, just by disabling those modules in the app.js file.    


Things to consider.

    Please wrap all your code in IIFE's (Immediately invokes Function expressions). It will make sure that we are not cluttering global namespace.